import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import DatePicker from 'react-native-date-picker';
import {styles} from './styles';

const DateTimePicker = props => {
  const [open, setOpen] = useState(false);
  // const [date, setDate] = useState(new Date());

  return (
    <View style={styles.pickerContainer}>
      {props.title != null && (
        <View style={styles.rowTitle}>
          <Text allowFontScaling={false} style={styles.title}>
            {props.title}
          </Text>
          {props.isRequire && (
            <Text allowFontScaling={false} style={styles.textRequire}>
              *
            </Text>
          )}
        </View>
      )}
      <TouchableOpacity
        style={styles.container}
        onPress={() => {
          setOpen(true);
        }}>
        <Text allowFontScaling={false} style={styles.text}>
          {`${props.date.getUTCDate()} - ${
            props.date.getUTCMonth() + 1
          } - ${props.date.getUTCFullYear()}`}
        </Text>
      </TouchableOpacity>
      <DatePicker
        style={styles.picker}
        modal
        mode={'date'}
        open={open}
        textColor="#000"
        date={props.date}
        onConfirm={dateTemp => {
          setOpen(false);
          props.setDate(new Date(dateTemp));
        }}
        onCancel={() => {
          setOpen(false);
        }}
      />
    </View>
  );
};

export default DateTimePicker;
