import {Dimensions, StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    borderRadius: 6,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    borderColor: '#acacac',
    borderWidth: 1,
  },
  text: {
    // flex: 1,
    color: '#000',
    fontSize: 16,
  },
  picker: {},
  title: {
    fontSize: 14,
    color: 'black',
    marginBottom: 5,
    marginTop: 10,
  },
  rowTitle: {
    flexDirection: 'row',
  },
  textRequire: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'red',
    marginBottom: 5,
    marginTop: 8,
    marginLeft: 3,
  },
});
