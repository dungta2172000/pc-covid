import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 20,
  },
  radio: {
    height: 20,
    width: 20,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inside: {
    height: 8,
    width: 8,
    borderRadius: 6,
    backgroundColor: '#000',
  },
  text: {
    marginStart: 8,
    color: '#000',
    fontSize: 16,
  },
});
