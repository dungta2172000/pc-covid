import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {styles} from './styles';

const RadioButton = props => {
  const onPress = value => {
    props.onChange(props.name, value);
  };

  return (
    <View style={[styles.row, props.style]}>
      {props.items.map((item, index) => (
        <TouchableOpacity
          style={styles.container}
          key={index}
          onPress={() => onPress(item.value)}>
          <View style={styles.radio}>
            {props.value === item.value ? <View style={styles.inside} /> : null}
          </View>
          <Text allowFontScaling={false} style={styles.text}>
            {item.label}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default RadioButton;
