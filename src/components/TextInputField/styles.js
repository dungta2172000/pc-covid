import {Dimensions, StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  columnContainer: {
    flexDirection: 'column',
    marginBottom: 10,
  },
  rowContainer: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    borderWidth: 1,
    alignItems: 'center',
    borderRadius: 8,
    borderColor: '#acacac',
    height: 50,
    backgroundColor: 'white',
  },
  textInput: {
    flex: 1,
    fontSize: 14,
    color: 'black',
    padding: 0,
    backgroundColor: '#fff',
    height: 40,
    borderRadius: 6,
    paddingHorizontal: 8,
    borderColor: '#acacac',
    borderWidth: 1,
  },
  title: {
    fontSize: 14,
    color: 'black',
    marginBottom: 5,
    marginTop: 10,
  },
  error: {
    borderColor: 'red',
  },
  rowTitle: {
    flexDirection: 'row',
  },
  textRequire: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'red',
    marginBottom: 5,
    marginTop: 8,
    marginLeft: 3,
  },
});
