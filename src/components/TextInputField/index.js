import React from 'react';
import {View, Text, TextInput} from 'react-native';
import {styles} from './styles';

const TextInputField = props => {
  return (
    <View style={styles.columnContainer}>
      {props.title != null && (
        <View style={styles.rowTitle}>
          <Text allowFontScaling={false} style={styles.title}>
            {props.title}
          </Text>
          {props.isRequire && (
            <Text allowFontScaling={false} style={styles.textRequire}>
              *
            </Text>
          )}
        </View>
      )}
      <TextInput
        placeholderTextColor="#b3b3b3"
        allowFontScaling={false}
        style={styles.textInput}
        placeholder={props.hint}
        onBlur={props.onBlur}
        value={props.value}
        secureTextEntry={props.isPassword === true ? !show : props.isPassword}
        onChangeText={props.onTextChange}
        editable={props.editable}
      />
    </View>
  );
};

export default TextInputField;
