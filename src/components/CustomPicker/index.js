import React from 'react';
import {View, TextInput} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import {styles} from './styles';

const CustomPicker = props => {
  return (
    <View>
      <ModalSelector
        data={props.option}
        onChange={option => {
          props.onChange(option.label);
        }}
        accessible={true}
        cancelText="Cancel"
        cancelTextStyle={styles.cancelText}
        cancelContainerStyle={styles.cancelContainer}
        cancelTextPassThruProps={{allowFontScaling: false}}
        selectTextPassThruProps={{allowFontScaling: false}}
        optionContainerStyle={styles.optionContainer}
        optionTextPassThruProps={{allowFontScaling: false}}
        optionTextStyle={styles.optionText}>
        <View style={styles.selectContainer}>
          <TextInput
            style={styles.selectText}
            allowFontScaling={false}
            value={props.value}
            editable={false}
            placeholder={props.placeholder}
            placeholderTextColor="#73818E"
          />
        </View>
      </ModalSelector>
    </View>
  );
};

export default CustomPicker;
