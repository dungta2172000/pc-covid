import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  itemStyle: {
    justifyContent: 'flex-start',
  },
  componentStyle: {
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000',
  },
  container: {
    height: 50,
    borderRadius: 10,
  },
  placeholder: {
    fontSize: 16,
    color: '#acacac',
  },
  optionContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    paddingVertical: 5,
  },
  optionText: {
    textAlign: 'left',
    color: '#000',
  },
  selectContainer: {
    alignItems: 'flex-start',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 10,
    backgroundColor: '#fff',
  },
  selectText: {
    textAlign: 'left',
    paddingVertical: 10,
    paddingHorizontal: 12,
    color: '#000',
  },
  cancelContainer: {
    backgroundColor: '$white',
    borderRadius: 10,
    overflow: 'hidden',
  },
  cancelText: {
    fontSize: 16,
    paddingVertical: 5,
  },
});
