import React, {useState} from 'react';
import {
  ScrollView,
  Text,
  TouchableOpacity,
  SafeAreaView,
  Button,
  View,
} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {StyleSheet} from 'react-native';
import {
  collection,
  getDocs,
  limit,
  orderBy,
  query,
  where,
  getDoc,
  doc,
} from 'firebase/firestore';
import {db} from '../../../firebase/firebase-config';
import {styles} from '../HealthForm/styles';

// const styles = StyleSheet.create({
//   centerText: {
//     flex: 1,
//     fontSize: 18,
//     padding: 32,
//     color: '#777',
//   },
//   textBold: {
//     fontWeight: '500',
//     color: '#000',
//   },
//   buttonText: {
//     fontSize: 21,
//     color: 'rgb(0,122,255)',
//   },
//   buttonTouchable: {
//     padding: 16,
//   },
// });

export const Scanner = ({navigation}) => {
  const [state, setState] = useState(null);

  const handleBack = () => {
    navigation.goBack();
  };

  const onRead = async val => {
    const personId = val.data;
    //get person data
    try {
      const personSnapshot = await getDoc(doc(db, 'person', personId));
      const personData = personSnapshot.data();
      const querySnapshot = await getDocs(
        query(
          collection(db, 'form'),
          where('person_id', '==', personId),
          orderBy('createAt', 'desc'),
          limit(1),
        ),
      );

      //getDeclare data
      let dataDeclare;
      querySnapshot.forEach(docTemp => {
        dataDeclare = docTemp.data();
      });

      // console.log('date_of_birth : ', personData.date_of_birth);

      if (personData && dataDeclare) {
        setState({...personData, ...dataDeclare});
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <SafeAreaView>
      <Button onPress={handleBack} title="back" />
      <ScrollView>
        <QRCodeScanner
          onRead={onRead}
          topContent={
            <Text style={styles.centerText}>
              Go to
              <Text style={styles.textBold}>wikipedia.org/wiki/QR_code</Text> on
              your computer and scan the QR code.
            </Text>
          }
          bottomContent={
            state ? (
              <View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    {'\n'}
                    Họ và tên : {state.name}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    CCCD : {state.person_id}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    Giới tính : {state.gender}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    Ngày sinh : {state.date_of_birth.toDate().toDateString()}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    Điện thoại : {state.phone_number}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    Số thẻ bhyt : {state.bhyt}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text allowFontScaling={false} style={styles.title2}>
                    Nơi cư trú :
                    {`${state.address}, ${state.commune}, ${state.district}, ${state.city}`}
                  </Text>
                </View>

                <View style={styles.rowBg}>
                  <View style={styles.row}>
                    <Text allowFontScaling={false} style={styles.title}>
                      Di chuyển ra ngoài thành phố sinh sống:{' '}
                      {state.isOutCity ? 'Có' : 'Không'}
                    </Text>
                  </View>
                </View>
                <View style={styles.rowBg}>
                  <View style={styles.row}>
                    <Text allowFontScaling={false} style={styles.title}>
                      Có dấu hiệu bệnh gần đây: {state.Sick ? 'Có' : 'Không'}
                    </Text>
                  </View>
                </View>
                <View style={styles.rowBg}>
                  <View style={styles.row}>
                    <Text allowFontScaling={false} style={styles.title}>
                      Trong vòng 14 ngày qua, lịch trình tiếp xúc nghi ngờ
                    </Text>
                  </View>
                  <Text allowFontScaling={false}>
                    Người bệnh hoặc nghi ngờ mắc bệnh Covid-19 :{' '}
                    {state.contact.covider ? 'Có' : 'Không'}
                  </Text>
                  <Text allowFontScaling={false}>
                    Người từ nước có bệnh Covid :{' '}
                    {state.contact.foreigner ? 'Có' : 'Không'}
                  </Text>
                  <Text allowFontScaling={false}>
                    Người bệnh có biểu hiện sốt, họ, khó thở, viêm phổi :{' '}
                    {state.contact.sicker ? 'Có' : 'Không'}
                  </Text>
                </View>
              </View>
            ) : (
              <View>
                <Text allowFontScaling={false} style={styles.title2}>
                  {'\n'}
                  Scan Qr code to get information
                </Text>
              </View>
            )
          }
        />
      </ScrollView>
    </SafeAreaView>
  );
};
