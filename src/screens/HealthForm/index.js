import React, {useState, useRef} from 'react';
import {View, Text, ScrollView, Button} from 'react-native';
import {AppBar} from '../../components/AppBar';
import CustomPicker from '../../components/CustomPicker';
import DateTimePicker from '../../components/DateTimePicker';
import TextInputField from '../../components/TextInputField';
import {styles} from './styles';
import * as yup from 'yup';
import {Formik} from 'formik';
import RadioButton from '../../components/RadioButton';
import {db} from '../../../firebase/firebase-config';
import {collection, addDoc, Timestamp, setDoc, doc} from 'firebase/firestore';

const schema = yup.object().shape({
  name: yup.string().required('Vui lòng nhập tên'),
  person_id: yup.string().required('Vui lòng nhập số CCCD'),
  phone_number: yup.string().required('Vui lòng nhập số di động'),
  city: yup.string().required('Vui lòng nhập tỉnh/thành phố'),
  district: yup.string().required('Vui lòng nhập quận/huyện'),
  commune: yup.string().required('Vui lòng nhập phường/xã'),
  address: yup.string().required('Vui lòng nhập địa chỉ'),
});

const GENDER_OPTION = [
  {label: 'Nam', key: 1},
  {label: 'Nữ', key: 2},
  {label: 'Khác', key: 3},
];
const YES_NO_OPTION = [
  {label: 'Không', value: false},
  {label: 'Có', value: true},
];

const NATION_OPTION = [
  {label: 'Việt Nam', key: 1},
  {label: 'Australida', key: 2},
  {label: 'Khác', key: 0},
];

export const HealthForm = ({navigation}) => {
  const [nation, setNation] = useState('');
  const [gender, setGender] = useState('Nam');
  const [expanded, setExpanded] = useState(false);
  const [date, setDate] = useState(new Date());

  const form = useRef(null);
  const formData = useRef({
    name: '',
    person_id: '',
    phone_number: '',
    bhyt: '',
    email: '',
    city: '',
    district: '',
    commune: '',
    address: '',
    isOutCity: false,
    isSick: false,
    covider: false,
    foreigner: false,
    sicker: false,
  });

  const handleBack = () => {
    navigation.goBack();
  };

  // const handleGetData = async () => {
  //   // lấy data về bằng personId, gắn vào các ô khai báo
  //   // const collectionList = collection(db)async function getCities(db) {
  //   const personCol = collection(db, 'person');
  //   const personSnapshot = await getDocs(personCol);
  //   const personList = personSnapshot.docs.map(doc => doc.data());
  //   console.log(personList);
  // };

  const onToggleForm = () => {
    setExpanded(!expanded);
  };

  const onSubmit = async values => {
    const {
      name,
      person_id,
      phone_number,
      bhyt,
      email,
      city,
      district,
      commune,
      address,
      isOutCity,
      isSick,
      covider,
      foreigner,
      sicker,
    } = values;
    // submit value data here
    const formDeclareTemplate = {
      contact: {
        covider,
        foreigner,
        sicker,
      },
      isOutCity,
      isSick,
      person_id,
      createAt: Timestamp.fromDate(new Date()),
    };

    //todo : check tồn tại id, nếu chưa có thì tạo person mới
    try {
      await setDoc(doc(db, 'person', person_id), {
        address,
        date_of_birth: date,
        email,
        gender,
        name,
        phone_number,
        bhyt,
        city,
        district,
        commune,
      });

      const result = await addDoc(collection(db, 'form'), formDeclareTemplate);
      if (result) {
        console.log(result.id);
        handleBack();
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <Button onPress={handleBack} title="back" />
      <AppBar
        title="Khai báo y tế"
        left-button="arrow-back-outline"
        action={handleBack}
      />
      <ScrollView style={styles.formContainer}>
        <View style={styles.formTop}>
          <View style={styles.flex}>
            <Text style={styles.bold}>Nguồn:</Text>
            <Text style={styles.formTopLink} onPress={handleBack}>
              Hướng dẫn tờ khai y tế; Mẫu tờ khai y tế
            </Text>
          </View>
          <Text style={styles.warning}>
            Khuyến cáo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam và
            có thể xử lý hình sự
          </Text>
        </View>
        <View style={styles.formHeader}>
          <Text style={styles.username} allowFontScaling={false}>
            TRẦN ANH DŨNG
          </Text>
          <Text style={styles.toggleText} onPress={onToggleForm}>
            {expanded ? 'Rút gọn' : 'Chi tiết'}
          </Text>
        </View>
        <View>
          <Formik
            validationSchema={schema}
            initialValues={formData.current}
            onSubmit={onSubmit}
            innerRef={instance => {
              form.current = instance;
            }}
            enableReinitialize>
            {({
              setFieldValue,
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
            }) => {
              return (
                <>
                  {expanded && (
                    <View>
                      <Text allowFontScaling={false}>CCCD chờ xác thực</Text>
                      <View style={styles.formBody}>
                        <View style={styles.row}>
                          <Text allowFontScaling={false} style={styles.title}>
                            Thông tin cá nhân
                          </Text>
                          <Text
                            allowFontScaling={false}
                            style={styles.requireText}>
                            *
                          </Text>
                        </View>
                        <TextInputField
                          title="Họ tên(Ghi chữ IN HOA)"
                          onBlur={handleBlur('name')}
                          value={values.name}
                          onTextChange={handleChange('name')}
                          isRequire={true}
                        />
                        {errors.name && (
                          <Text style={styles.requireText}>{errors.name}</Text>
                        )}
                        <TextInputField
                          title="CMND/CCCD/Hộ chiếu"
                          isRequire={true}
                          onBlur={handleBlur('person_id')}
                          value={values.person_id}
                          onTextChange={handleChange('person_id')}
                        />
                        {errors.person_id && (
                          <Text style={styles.requireText}>
                            {errors.person_id}
                          </Text>
                        )}
                        <View style={styles.row}>
                          <Text allowFontScaling={false}>Giới tính</Text>
                          <Text
                            allowFontScaling={false}
                            style={styles.requireText}>
                            *
                          </Text>
                        </View>
                        <CustomPicker
                          option={GENDER_OPTION}
                          value={gender}
                          onChange={value => setGender(value)}
                        />
                        <DateTimePicker
                          title="Ngày sinh"
                          isRequire={true}
                          date={date}
                          setDate={setDate}
                        />
                      </View>
                      <TextInputField
                        title="Điện thoại"
                        isRequire={true}
                        onBlur={handleBlur('phone_number')}
                        value={values.phone_number}
                        onTextChange={handleChange('phone_number')}
                      />
                      {errors.phone_number && (
                        <Text style={styles.requireText}>
                          {errors.phone_number}
                        </Text>
                      )}
                      <TextInputField
                        hint="Số thẻ BHYT"
                        onBlur={handleBlur('bhyt')}
                        value={values.bhyt}
                        onTextChange={handleChange('bhyt')}
                      />
                      {errors.bhyt && (
                        <Text style={styles.requireText}>{errors.bhyt}</Text>
                      )}
                      <TextInputField
                        hint="Email"
                        isRequire={true}
                        onBlur={handleBlur('email')}
                        value={values.email}
                        onTextChange={handleChange('email')}
                      />
                      {errors.email && (
                        <Text style={styles.requireText}>{errors.email}</Text>
                      )}
                      <CustomPicker
                        option={NATION_OPTION}
                        value={nation}
                        placeholder={'Chọn quốc tịch'}
                        onChange={value => {
                          setNation(value);
                        }}
                      />
                      <View style={styles.row}>
                        <Text allowFontScaling={false} style={styles.title}>
                          Nơi cư trú
                        </Text>
                        <Text
                          allowFontScaling={false}
                          style={styles.requireText}>
                          *
                        </Text>
                      </View>
                      <TextInputField
                        title="Tỉnh/thành phố"
                        isRequire={true}
                        onBlur={handleBlur('city')}
                        value={values.city}
                        onTextChange={handleChange('city')}
                      />
                      {errors.city && (
                        <Text style={styles.requireText}>{errors.city}</Text>
                      )}
                      <TextInputField
                        title="Quận/huyện"
                        isRequire={true}
                        onBlur={handleBlur('district')}
                        value={values.district}
                        onTextChange={handleChange('district')}
                      />
                      {errors.district && (
                        <Text style={styles.requireText}>
                          {errors.district}
                        </Text>
                      )}
                      <TextInputField
                        title="Phường xã"
                        isRequire={true}
                        onBlur={handleBlur('commune')}
                        value={values.commune}
                        onTextChange={handleChange('commune')}
                      />
                      {errors.commune && (
                        <Text style={styles.requireText}>{errors.commune}</Text>
                      )}
                      <TextInputField
                        title="Số nhà, phố, tổ dân phố/thôn/đội"
                        isRequire={true}
                        onBlur={handleBlur('address')}
                        value={values.address}
                        onTextChange={handleChange('address')}
                      />
                      {errors.address && (
                        <Text style={styles.requireText}>{errors.address}</Text>
                      )}
                    </View>
                  )}
                  <View style={styles.rowBg}>
                    <View style={styles.row}>
                      <Text allowFontScaling={false} style={styles.title}>
                        Trong vòng 14 ngày qua, Anh/Chị có đến khu vực, tỉnh,
                        thành phố, quốc gia/vùng lãnh thổ nào (Có thể đi qua
                        nhiều nơi)
                      </Text>
                      <Text allowFontScaling={false} style={styles.requireText}>
                        *
                      </Text>
                    </View>
                    <RadioButton
                      name="isOutCity"
                      items={YES_NO_OPTION}
                      onChange={setFieldValue}
                      value={values.isOutCity}
                    />
                  </View>
                  <View style={styles.rowBg}>
                    <View style={styles.row}>
                      <Text allowFontScaling={false} style={styles.title}>
                        Trong vòng 14 ngày qua, Anh/Chị có thấy xuất hiện ít
                        nhất 1 trong các dấu hiệu: sốt, ho, khó thở, viêm phổi,
                        đau họng, mệt mỏi, thay đổi vị giác không?
                      </Text>
                      <Text allowFontScaling={false} style={styles.requireText}>
                        *
                      </Text>
                    </View>
                    <RadioButton
                      name="isSick"
                      items={YES_NO_OPTION}
                      onChange={setFieldValue}
                      value={values.isSick}
                    />
                  </View>
                  <View style={styles.rowBg}>
                    <View style={styles.row}>
                      <Text allowFontScaling={false} style={styles.title}>
                        Trong vòng 14 ngày qua, Anh/Chị có tiếp xúc với:
                      </Text>
                      <Text allowFontScaling={false} style={styles.requireText}>
                        *
                      </Text>
                    </View>
                    <Text allowFontScaling={false}>
                      Người bệnh hoặc nghi ngờ mắc bệnh Covid-19
                    </Text>
                    <RadioButton
                      name="covider"
                      items={YES_NO_OPTION}
                      onChange={setFieldValue}
                      value={values.covider}
                    />
                    <Text allowFontScaling={false}>
                      Người từ nước có bệnh Covid
                    </Text>
                    <RadioButton
                      name="foreigner"
                      items={YES_NO_OPTION}
                      onChange={setFieldValue}
                      value={values.foreigner}
                    />
                    <Text allowFontScaling={false}>
                      Người bệnh có biểu hiện sốt, họ, khó thở, viêm phổi
                    </Text>
                    <RadioButton
                      name="sicker"
                      items={YES_NO_OPTION}
                      onChange={setFieldValue}
                      value={values.sicker}
                    />
                  </View>
                  <View style={styles.formBottom}>
                    <Text style={styles.warning}>
                      Dữ liệu bạn cung cấp hoàn toàn bảo mật và chỉ phục vụ cho
                      việc phòng, chống dịch, thuộc quản lý của ban chỉ đạo quốc
                      gia về Phòng, chống dịch Covid-19. Khi bạn ấn nút "Gửi" là
                      bạn đã hiểu và đồng ý.
                    </Text>
                  </View>
                  <View style={styles.btnContainer}>
                    <Button title="Gửi tờ khai" onPress={handleSubmit} />
                    {/* <Button title="Get data" onPress={handleGetData} /> */}
                  </View>
                </>
              );
            }}
          </Formik>
        </View>
      </ScrollView>
    </View>
  );
};
