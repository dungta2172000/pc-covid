import {Dimensions, StyleSheet} from 'react-native';

const size = Dimensions.get('screen');

export const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    paddingBottom: 20,
    flex: 1,
    backgroundColor: '#fff',
  },
  formContainer: {
    paddingHorizontal: 10,
    // alignItems: 'center',
  },
  formHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  formBody: {
    borderColor: '#000',
    borderWidth: 1,
    padding: 10,
    borderRadius: 8,
  },
  row: {
    flexDirection: 'row',
  },
  requireText: {
    color: 'red',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#000',
  },
  title2: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#000',
  },
});
