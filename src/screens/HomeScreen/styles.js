import {Dimensions, StyleSheet} from 'react-native';

const size = Dimensions.get('screen');

export const styles = StyleSheet.create({
  background: {
    paddingTop: 50,
    flex: 1,
    width: '100%',
    height: '100%',
    zIndex: 1,
    position: 'absolute',
    backgroundColor: '#ECECEC',
    paddingVertical: 30,
    paddingHorizontal: 20,
  },
  container: {
    paddingTop: 10,
    zIndex: 2,
    backgroundColor: '#30B55C',
    borderRadius: 30,
    height: '100%',
    justifyContent: 'space-between',
    overflow: 'hidden',
  },
  infoContainer: {
    padding: 30,
    width: '100%',
    alignItems: 'center',
  },
  cardName: {
    fontSize: 25,
    color: 'white',
    fontWeight: '500',
  },
  location: {
    fontSize: 17,
    color: 'white',
    fontWeight: '400',
  },
  qrCode: {
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 30,
    marginTop: 20,
    marginBottom: 15,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 25,
    color: 'black',
  },
  info: {
    color: 'black',
    fontSize: 20,
  },
  function: {
    backgroundColor: 'white',
    height: 'auto',
    width: '100%',
    alignSelf: 'flex-end',
  },
  bottomBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'relative',
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  scanQr: {
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    zIndex: 2,
    // left: '50%',
    top: -40,
  },
  qrButton: {
    height: 50,
    width: 50,
    padding: 2,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#30B55C',
  },
  main: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 50,
    paddingHorizontal: 20,
  },
});
