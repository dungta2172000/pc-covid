import {
  collection,
  getDocs,
  limit,
  orderBy,
  query,
  where,
} from 'firebase/firestore';
import React from 'react';
import {Text, TouchableOpacity, View, SafeAreaView} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import {db} from '../../../firebase/firebase-config';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {styles} from './styles';

export const HomeScreen = ({navigation}) => {
  const goToHealthForm = () => {
    navigation.push('HealthForm');
  };

  // useEffect(() => {

  // }, [])

  const getData = async personId => {
    const querySnapshot = await getDocs(
      query(
        collection(db, 'form'),
        where('person_id', '==', personId),
        orderBy('createAt', 'desc'),
        limit(1),
      ),
    );
    let data;
    querySnapshot.forEach(doc => {
      data = doc.data();
    });
    console.log(data);
  };

  return (
    <View style={styles.background}>
      <View style={styles.container}>
        <View style={styles.infoContainer}>
          <Text allowFontScaling={false} style={styles.cardName}>
            THẺ THÔNG TIN COVID
          </Text>

          <Text allowFontScaling={false} style={styles.location}>
            Áp dụng tại Hà Nội
          </Text>
          <View style={styles.qrCode}>
            <QRCode value="017200000046" size={150} />
          </View>
          <Text allowFontScaling={false} style={styles.name}>
            TRẦN ANH DŨNG
          </Text>
          <Text allowFontScaling={false} style={styles.info}>
            Nam - 2000
          </Text>
        </View>
        <View style={styles.function}>
          <View style={styles.main}>
            <Text allowFontScaling={false}>Quản lý QR</Text>
            <TouchableOpacity onPress={goToHealthForm}>
              <View>
                {/* <Ionicons name={'arrow-back-outline'} size={25} /> */}
                <Text allowFontScaling={false}>Khai báo ý tế</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => getData('017200000046')}>
              <View>
                {/* <Ionicons name={'arrow-back-outline'} size={25} /> */}
                <Text>Gửi phản ánh</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.bottomBar}>
            <Text allowFontScaling={false}>Thông báo</Text>

            <TouchableOpacity onPress={() => navigation.push('Scanner')}>
              <View>
                {/* <Ionicons name={'arrow-back-outline'} size={25} /> */}
                <Text allowFontScaling={false}>Quét QR</Text>
              </View>
            </TouchableOpacity>

            <Text allowFontScaling={false}>Cài đặt</Text>
          </View>
        </View>
      </View>
    </View>
  );
};
