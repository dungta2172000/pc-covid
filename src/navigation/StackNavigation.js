import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {HomeScreen} from '../screens/HomeScreen';
import {HealthForm} from '../screens/HealthForm';
import {Scanner} from '../screens/Scanner';

const Stack = createNativeStackNavigator();

export const StackNavigation = ({navigation}) => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false, animationEnabled: true}}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="HealthForm" component={HealthForm} />
      <Stack.Screen name="Scanner" component={Scanner} />
    </Stack.Navigator>
  );
};
