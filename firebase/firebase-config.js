// Import the functions you need from the SDKs you need
import {initializeApp} from 'firebase/app';
import {getFirestore} from 'firebase/firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyDQry68OwRPP7VRNrzJrPP9EuVsgxzfk4o',
  authDomain: 'pc-covid-9e0ba.firebaseapp.com',
  projectId: 'pc-covid-9e0ba',
  storageBucket: 'pc-covid-9e0ba.appspot.com',
  messagingSenderId: '40609749628',
  appId: '1:40609749628:web:04b30b8bc1934db3c787f9',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
